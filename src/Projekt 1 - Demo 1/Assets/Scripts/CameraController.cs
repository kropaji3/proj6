using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float movementSpeed = 50;
    public float rotationSpeed = 50;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            transform.rotation = new Quaternion(0,0,0,1);
        }
        Vector3 translationVector = new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Fly") * 0.5f, Input.GetAxis("Vertical")) * movementSpeed * Time.deltaTime;
        Vector3 rotationVector = new Vector3(-Input.GetAxis("RotationX"), Input.GetAxis("RotationY"), -Input.GetAxis("RotationZ")) * rotationSpeed * Time.deltaTime;
        transform.Translate(translationVector, Space.Self);
        transform.Rotate(rotationVector, Space.Self);
    }
}
