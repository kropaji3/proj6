using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsViewScript : MonoBehaviour
{
    public Text posText;
    public Text rotText;
    public Text fpsText;

    public Transform cameraTransform;

    // Update is called once per frame
    void Update()
    {
        /*posText.text = "Position - |x: " + cameraTransform.position.x + 
            " |y: " + cameraTransform.position.y +
            " |z: " + cameraTransform.position.z;
        rotText.text = "Rotation - |x: " + cameraTransform.rotation.x +
            " |y: " + cameraTransform.rotation.y +
            " |z: " + cameraTransform.rotation.z;
        fpsText.text = "FPS - : " + (Time.frameCount / Time.time).ToString();*/

        posText.text = string.Format("POS - : X| {0:#.00} ; Y| {1:#.00} ; Z| {2:#.00} ",
            cameraTransform.position.x,
            cameraTransform.position.y,
            cameraTransform.position.z);
        rotText.text = string.Format("ROT - : X| {0:#.00} ; Y| {1:#.00} ; Z| {2:#.00} ",
            cameraTransform.eulerAngles.x,
            cameraTransform.eulerAngles.y,
            cameraTransform.eulerAngles.z);
        fpsText.text = string.Format("FPS - : {0:#.00}", (Time.frameCount / Time.time));
    }
}
