using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {
            LoadScene(0);
        } else if (Input.GetKey(KeyCode.F2))
        {
            LoadScene(1);
        } else if (Input.GetKey(KeyCode.F3))
        {
            LoadScene(2);
        } else if (Input.GetKey(KeyCode.F5))
        {
            LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

}
