using UnityEngine;
using UnityEngine.Profiling;

public class CameraRailScript : MonoBehaviour
{
    public GameObject cameraGameObject;
    public Transform[] railPoints;
    private int nextIndex = 0;
    private Vector3 newDestination;
    public float movementSpeed = 10;
    public float rotationSpeed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        cameraGameObject.transform.position = railPoints[0].position;
        cameraGameObject.transform.rotation = railPoints[0].rotation;
        newDestination = railPoints[0].position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(newDestination, cameraGameObject.transform.position) < movementSpeed/10.0f)
        {
            nextIndex++;
        }

        if (nextIndex < railPoints.Length)
        {
            newDestination = railPoints[nextIndex].position;
            
            Vector3 targetDirection = (newDestination - cameraGameObject.transform.position).normalized;
            Vector3 rotation = Vector3.RotateTowards(cameraGameObject.transform.forward,
                targetDirection,
                rotationSpeed * Time.deltaTime,
                0.0f);
            Debug.DrawRay(transform.position, targetDirection, Color.red);
            Vector3 moveVector = (newDestination - cameraGameObject.transform.position).normalized;
            cameraGameObject.transform.rotation = Quaternion.LookRotation(rotation);
            cameraGameObject.transform.Translate(movementSpeed * Time.deltaTime * moveVector, Space.World); 
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < railPoints.Length - 1; i++)
        {
            Debug.DrawLine(railPoints[i].position, railPoints[i + 1].position, Color.yellow);
        }
    }
}
