using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceLODScript : MonoBehaviour
{
    LODGroup lODGroup;
    // Start is called before the first frame update
    void Start()
    {
        lODGroup = GetComponent<LODGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        lODGroup.ForceLOD(0);
    }
}
