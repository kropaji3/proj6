using System.Text;
using Unity.Profiling;
using UnityEngine;
/**
 * 
 * Code taken from the Unity Documentation Scripting API
 * https://docs.unity3d.com/Manual/ProfilerRendering.html
 * https://docs.unity3d.com/ScriptReference/Unity.Profiling.ProfilerRecorder.html
 * 
 * */


public class ProfilerController: MonoBehaviour
{
    private static ProfilerController _instance;
    public static ProfilerController Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("ProfilerController is NULL");
            return _instance;
        }
    }

    string statsText;
    ProfilerRecorder mainThreadTimeRecorder;
    ProfilerRecorder renderThreadTimeRecorder;
    ProfilerRecorder setPassCallsRecorder;
    ProfilerRecorder drawCallsRecorder;
    ProfilerRecorder verticesRecorder;

    public TMPro.TextMeshProUGUI m_StatsText;

    static double GetRecorderFrameAverage(ProfilerRecorder recorder)
    {
        var samplesCount = recorder.Capacity;
        if (samplesCount == 0)
            return 0;

        double r = 0;
        unsafe
        {
            var samples = stackalloc ProfilerRecorderSample[samplesCount];
            recorder.CopyTo(samples, samplesCount);
            for (var i = 0; i < samplesCount; ++i)
                r += samples[i].Value;
            r /= samplesCount;
        }

        return r;
    }

    private void Awake()
    {
        _instance = this;
    }

    void OnEnable()
    {
        mainThreadTimeRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Internal, "Main Thread", 15);
        //renderThreadTimeRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Input, "Render Thread");
        setPassCallsRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Render, "SetPass Calls Count");
        drawCallsRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Render, "Draw Calls Count");
        verticesRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Render, "Vertices Count");
    }

    void OnDisable()
    {
        mainThreadTimeRecorder.Dispose();
        //renderThreadTimeRecorder.Dispose();
        setPassCallsRecorder.Dispose();
        drawCallsRecorder.Dispose();
        verticesRecorder.Dispose();
    }

    void Update()
    {
        var sb = new StringBuilder(500);
        sb.AppendLine($"Time: {Time.realtimeSinceStartup}");
        sb.AppendLine($"Frame: {Time.frameCount}");
        sb.AppendLine($"Frame Time: {GetRecorderFrameAverage(mainThreadTimeRecorder) * (1e-6f):F1} ms");
        //sb.AppendLine($"Frame Time: {renderThreadTimeRecorder.LastValue * (1e-6f):F1} ms");
        if (setPassCallsRecorder.Valid)
            sb.AppendLine($"SetPass Calls: {setPassCallsRecorder.LastValue}");
        if (drawCallsRecorder.Valid)
            sb.AppendLine($"Draw Calls: {drawCallsRecorder.LastValue}");
        if (verticesRecorder.Valid)
            sb.AppendLine($"Vertices: {verticesRecorder.LastValue}");
        m_StatsText.text = sb.ToString();
    }

    public void PrintStats(int checkpointIdx)
    {
        Debug.Log(checkpointIdx + " " + m_StatsText.text.ToString());
    }
}