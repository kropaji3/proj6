\documentclass{ctuthesis}

\ctusetup{
	%xdoctype = B,
	xfaculty = F3,
	mainlanguage = english,
	titlelanguage = english,
	title-english = {Performance Optimizations in Unity Engine},
	title-czech = {Výkonnostní optimalizace v Unity},
	department-english = {Department of Computer Graphics and Interaction},
	department-czech = {Katedra počítačové grafiky a interakce},
	author = {Jiří Kropáč},
	supervisor = {doc. Ing. Jiří Bittner, Ph.D.},
	supervisor-address = {},
	month = 5,
	year = 2023,
}

\ctuprocess

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{pgfplots}
\usepackage{url}

\begin{abstract-english}
In this project, I will describe the main methods the Unity engine uses to optimise builds performance and create two demos. I will test a chosen set of these methods on different game environments and hardware.
\end{abstract-english}

\begin{abstract-czech}
\end{abstract-czech}

\begin{document}

\maketitle

\chapter{Introduction}

A critical aspect of games, and software in general, is their performance. This can vary depending on many factors, for example, the hardware on which the software runs, the computational cost or the render difficulty. The game must run smoothly to ensure a satisfying user experience; in other words, optimising the game's performance is fundamental to its success. Fortunately, most primary optimising techniques are built in the Unity Engine and applied during build.

This paper will be divided into four parts: describing the built-in methods of the Unity Engine, describing methods supported by the engine, focusing on the visibility culling optimisation techniques and testing chosen techniques in the engine.

\section{Unity Engine}
The Unity Engine is a multi-platform game engine developed by Unity Technologies. The first version was released in 2005. The engine is used for 2D and 3D game development for all platforms, such as PCs, consoles and mobile devices. 

\chapter{Documentation}
The Unity Engine has several built-in methods to help optimise game software. During build, Unity performs basic optimisation for the target platform. Other methods need to be enabled or baked in the editor. Each method is well documented in the Unity manual. 

\section{Profiling}
Profiling helps the developer with finding bottlenecks that lower performance. Which helps furthermore optimise. It is recommended to profile from the early stages of development to the finished product, as it can help tackle performance issues much faster.

When profiling in the Unity editor, results are relative but serve as a good idea of how the game will perform. More critical is profiling on the target platform, keeping in mind the highest and lowest spec devices.

Instead of assuming performance issues, the Unity Profiler can more accurately detect the source. It also shows what exactly is happening in each frame.
\subsection{Profiler}
The Unity profiler is an instrumentation-based profiler, which means it profiles code timings explicitly wrapped in ProfilerMakers and helps detect causes of bottlenecks or freezes during run-time.

The profiler can monitor CPU usage, GPU usage, rendering, memory, audio, video, physics (3D and 2D), network, UI, global illumination and virtual texturing. These profiler modules can be enabled and disabled as needed, tracking only those critical for the game. Unnecessary module recording can impact performance and affect the results.

I am recording only the CPU and GPU usage and rendering to test occlusion culling.

In the detailed part of the profiler, the hierarchy, we see the different API calls in the selected frame. We can also find out if our game is CPU or GPU bound, which means that one unit needs to wait for the other to continue execution. If a game is CPU bound, the GPU waits and vice versa. The gfx.waitforpresent API call, when the CPU is ready to render the next frame but needs to wait for the GPU to present the next frame, in the profiler, will reveal if the computer is GPU bound or not.

\subsection{Frame Time}
The frame time is the length of a frame in milliseconds. We look at the CPU and GPU time to determine the frame time and choose the higher. It is more accurate than frames per second as it shows how many frames can fit in a second if each frame were the same length. To determine the frame time from the FPS we want to achieve, we divide 1000ms by the target FPS, as shown in this equation \ref{eq:frame PU time}.
\begin{equation}
    \label{eq:frame PU time}
    t_{cpu} = \frac{1000 ms}{FPS}
\end{equation}

\begin{tikzpicture}
\begin{axis}[
    axis lines=left,
    xlabel={frames per second},
    ylabel={milliseconds per frame},
]
\addplot[
domain=0:100,
samples=20,
color=green
]{1000/x};
\end{axis}
\end{tikzpicture}

For the three most popular target FPS counts, the frame times are defined as follows: 
\begin{center}
  \textbf{60fps -> 16ms; \\  30fps -> 32ms; \\(VR) 90fps -> 11ms }.  
\end{center}
With the frame time being more accurate, developers prefer to benchmark it over the frames per second.

\section{Levels of detail}
Level of detail is a rendering optimization technique that reduces the number of GPU operations required to render distant meshes. The main idea is to reduce the number of triangles rendered of a mesh depending on its distance from the camera. For this, we create from high-detail object copies that have decreasingly fewer faces, called LOD levels. As the camera moves further away, the high-detail mesh is replaced by lower-resolution meshes. Thus saving GPU operation and helping the performance of the scene.

\section{Visibility culling}
Visibility culling is an optimization technique that saves rendering performance. It happens for each camera in the scene, where it renders only what can be seen. There are two types of visibility culling that Unity uses: one, which doesn't render GameObjects outside of the view frustum, called frustum culling, and one, which doesn't render GameObjects that are hidden behind other objects, called occlusion culling.

\subsection{Frustum Culling}

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.65\linewidth]{Demo1/Screenshot_demo1_fustrum_culling.png}
    \caption{Demo 1}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.65\linewidth]{Demo2/Screenshot_demo2_fustrum_culling.png}
    \caption{Demo 2}
    \end{subfigure}
    \caption{Frustum Culling}
    \label{fig:Frustum Culling}
\end{figure}

Frustum Culling is performed automatically on every camera. The culling is done by layers first, rendering only the GameObjects on the layers the camera uses, then removing any of the GameObjects outside the camera frustum.

Unity recommends organizing GameObjects into different Layers. There is a maximum of 32 layers, each of which can be assigned a value less than the farClipPlane in the layerCullDistances array. There is also a possibility to manually set per-layer culling distances using Camera.layerCullDistances, which allows for culling objects closer to the Camera than the default farClipPlane.

\subsection{Occlusion Culling}

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.65\linewidth]{Demo1/Screenshot_demo1_occlusion_culling.png}
    \caption{Demo 1}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.65\linewidth]{Demo2/Screenshot_demo2_occlusion_culling.png}
    \caption{Demo 2}
    \end{subfigure}
    \caption{Occlusion Culling}
    \label{fig:Occlusion Culling}
\end{figure}

Occlusion culling removes any GameObjects occluded by other GameObject, such as those that the Camera doesn't see. Thus lowering the render cost even more after frustum culling. On the other hand, it is a baked process. 

Unity bakes the data during build. This data, which takes some disk space, and costs CPU time and RAM access, can also be baked manually in the editor. This way, the developer can control the parameters to ensure the best performance at a low data size.

Unity uses this data during run time to determine what the Camera sees. The scene is divided into small cells, and data is generated describing the geometry within the cell and visibility between adjacent cells. 

\subsection{Occlusion Data}
For occlusion culling to work, we need first to bake the data that Unity will use to calculate what to render and what not. In the baking window, three different parameters can be set. Each impacts how fast the data will be baked, how much size the data will take and how the rendering will be affected.

The first parameter, Smallest Occluder (default set to 5), indicates the size, in meters, of the smallest GameObject that can occlude other GameObjects. The smaller the number, the longer the data need to be baked, and the bigger the data size on the disk will be. On the other hand, fewer objects can be rendered, as even a tiny object can occlude objects behind it.

The second parameter, Smallest Hole (default set to 0.25), indicates the diameter, in meters, of the smallest hole through which the Camera can see. 

For both of these parameters, in general, for the smallest data file size and fastest bake times, we need to find the highest number that gives us the best render and performance result. 

The third and last parameter, Backface Threshold (default set to 100), can be set smaller if we need to reduce the size of the baked data. But it can lead to visual artifacts. It denotes the limit percentage of backfaces a visible occluder geometry can have so it is not discarded. The default value of 100 never removes any area.

As the baked data works only in the scene it was baked, we can fine-tune each parameter in the different scenes such that they run smoothly. A larger scene would need a higher parameter for the smallest occluder than a smaller scene. It also varies on the GameObjects present in each scene. This means each scene needs to have its occlusion culling data.

\chapter{Test demos}
In Unity, I created two new 3D projects using the core 3D template and the 2020.3.41f1 LTS version of the engine. Each demo will be focused on a different game environment to test their interaction of the baked occlusion culling. 

All the demos were created and sampled on my laptop with these hardware specifications:\\ 

DELL G5 5587 \\
Processor:	Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz   2.21 GHz \\
Graphics: NVIDIA
RAM	32,0 GB \\
64bit OS \\



\section{Demo 1 - Terrain and Culling}
The first demo will test the interaction of terrain GameObject and Occlusion Culling.

Each scene has the default setting for the camera component. After entering the play mode, the Camera will follow a predetermined path. This way, I can compare the performance from the same sample. The only scene where the Camera doesn't follow a path is the second one because of the large size of the terrain.

\subsection{Scene 1}

This scene will serve as a basic test of how the occlusion data works with the terrain. The terrain tile size, the camera setting and the parameters for baking the occlusion data are all set to default values.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.6\linewidth]{Demo1/Screenshot_demo1_scene1_sceneview_1.png}
    \caption{Scene view}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[height=0.6\linewidth]{Demo1/Screenshot_demo1_scene1_gameview_1.png}
    \caption{Game view}
    \end{subfigure}
    \caption{Demo 1, Scene 1, screenshots of the scene setup}
    \label{fig:Demo 1, Scene 1, screenshots 1}
\end{figure}

A village made of different buildings is inside this mountain valley covered by many trees, see Figure \ref{fig:Demo 1, Scene 1, screenshots 1}. 

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene1_compOcC_1.png}
    \caption{Beginning}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene1_compOcC_2.png}
    \caption{Mid progress}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene1_resultOcC_2.png}
    \caption{Result}
    \end{subfigure}
    \caption{Demo 1, Scene 2, screenshots of different stages of baking the occlusion culling data}
    \label{fig:Demo 1, Scene 1, screenshots 2}
\end{figure}

The baking process took 30 minutes to complete, see Figure \ref{fig:Demo 1, Scene 1, screenshots 2}.
\subsubsection{Results}
The scene was divided into small cells. Each cell is rendered if the Camera would see objects in them. The Camera sends rays in the view frustum to calculate which cell it sees. The terrain is also not rendered in its entirety only the visible parts. The objects behave as expected.
 
\subsection{Scene 2}

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene2_sceneview_1.png}
    \caption{Overview of the terrain}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene2_sceneview_2.png}
    \caption{View of the valley}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene2_sceneview_3.png}
    \caption{View with the location of the Camera}
    \end{subfigure}
    \caption{Demo 1, Scene 2, screenshots of the terrain and buildings}
    \label{fig:Demo 1, Scene 2, screenshots 1}
\end{figure}

This scene is a mountain valley filled with trees and bushes. In the valley lies a small number of villages composed of different types of houses and three cities, a grid of destroyed skyscrapers, see Figure \ref{fig:Demo 1, Scene 2, screenshots 1}. The terrain tile is ten times bigger than the default size.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene2_sceneview_4.png}
    \caption{Overview of the scene with the inspector}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene2_gameview_1.png}
    \caption{Game view}
    \end{subfigure}
    \caption{Demo 1, Scene 2, screenshots of the camera setup}
    \label{fig:Demo 1, Scene 2, screenshots 2}
\end{figure}

The Camera is placed on the outskirts of one of the villages. It is moved in the air to have a better view. I am using the default setting of the camera component. The Camera sees some of the houses in the village in the foreground and the background: destroyed skyscrapers on the left and a hill in the centre.

\subsubsection{Results}
In this first version of the scene, on the default setting of the occlusion parameters, the baking process took more than 11 hours and resulted in an error.

After several tries, I concluded that this large-scale terrain was too big for my computer to bake the occlusion data.

\subsection{Scene 3}
I reduced the terrain size in the third scene compared to the terrain tile in the second scene. This time the terrain tile is only five times bigger than the default size. I used the Unity tree wizard to mass-place 1000000 trees \ref{fig:Demo 1, Scene 3, screenshot 1}. The terrain is more varied, with several valleys and mountain ranges.
\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene3_trees.png}
        \caption{Mass place trees}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\linewidth}
        \includegraphics[width=\linewidth]{Demo1/Screenshot_demo1_scene3_sceneview1.png}
        \caption{Scene view}
    \end{subfigure}
    \caption{Demo 1, Scene 3, screenshot of tree placing}
    \label{fig:Demo 1, Scene 3, screenshot 1}
\end{figure}
\subsubsection{Results}
This scene had similar problems as the second. The terrain was still too large. Even with the smallest occluder set to 500, the baking process resulted in an error. The only occluders present in the scene would be the mountain ranges.

\subsection{Conclusion}
The terrain tile is divided into small tiles, and these are then culled when necessary. Other GameObjects, like the trees and buildings, behave as expected. Occlusion culling on large terrains takes too much time to bake and place on a disk. As Unity default uses frustum culling, it is the only effective culling on large plains. It also shows that occlusion culling will be more effective in smaller-scale scenes or with a high density of occluding objects.

\section{Demo 2}
In the second scene, I created a labyrinth-like structure of rooms of different sizes and narrow corridors connecting them. The narrow and short corridors are perfect for testing the occlusion culling. Dividing this complex into small chunks and pieces was crucial as a single object would always be rendered. It also shows that Unity culls whole GameObjects and not only vertices.  

This project's small scale will help me study the impact of each parameter used for occlusion culling baking.

\subsection{Profiling}
In the main thread, I looked for the gfx.waitforpresent API call, I have found it, which means this demo is GPU bound.

In the table \ref{tab:occlusion table}, we can see the impact of each parameter on the size of the bake data file. When the first two parameters are set to a smaller value, the size of the file is more significant. For my scene, the third parameter doesn't impact baking or data size. The result of the first two rows can be visualized in \ref{fig:DefOccParam}. The smaller size of the smallest occluder creates smaller occlusion areas, the blue wired boxes, resulting in fewer parts of the scene to render. 

\subsubsection{Test 1}
\begin{table}[h]
    \caption{Occlusion Baking Parameters}
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
         \textbf{Smallest Occluder} & \textbf{Smallest Hole} & \textbf{Backface Threshold} & \textbf{Occlusion Data Size} \\ \hline\hline
         \textbf{5} & \textbf{0.25} & \textbf{100} & 63.9KB \\ \hline
         1 & \textbf{0.25} & \textbf{100} & 416.7KB \\
         1 & 0.10 & \textbf{100} & 367.9KB \\
         \textbf{5} & 0.10 & \textbf{100} & 79.2KB\\
         8 & 0.50 & \textbf{100} & 16.9KB \\
         9 & \textbf{0.25} & \textbf{100} & 19.3KB \\\hline
         \multicolumn{4}{l}{Default values are bolded}
    \end{tabular}
    \label{tab:occlusion table}
\end{table}

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[trim=450 60 450 200,clip,width=\linewidth]{Demo2/Screenshot_demo2_defaultOccluder.png}
    \caption{Baked - ( 5, 0.25, 100)}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[trim=450 60 450 200,clip,width=\linewidth]{Demo2/Screenshot_demo2_defaultOccluder_result.png}
    \caption{Runtime}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[trim=450 60 450 200,clip,width=\linewidth]{Demo2/Screenshot_demo2_smallerOccluder.png}
    \caption{Baked - ( 1, 0.25, 100)}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
        \includegraphics[trim=450 60 450 200,clip,width=\linewidth]{Demo2/Screenshot_demo2_smallerOccluder_result.png}
    \caption{Runtime}
    \end{subfigure}
    \caption{Occlusion Parameters}
    \label{fig:DefOccParam}
\end{figure}

I ran the scene with the profiler connected and recorded performance data for each baked data. I chose to look at the 417th frame of each sample, writing down the CPU and GPU time of the frame. The frames per second were calculated by using the equation \ref{eq:FPS} derived from \ref{eq:frame PU time}
\begin{equation}
    FPS = \frac{1000}{t_{cpu} [ms]} 
    \label{eq:FPS}
\end{equation} 
and rounding down the result to a whole number. Results are in the \ref{tab:profiler table frame} table. After occlusion culling, the FPS count increased from 44 to 91 using the default values. Because the FPS differs for each data, the 417th frame is different. When the FPS is higher, the frame will come earlier. It also shows how effective the occlusion culling is with the right parameters, dividing the frame time by 2. The second conclusion is that smaller values don't mean better results, as the engine needs to calculate the data more often, frequently changing the game objects that are rendered.

\begin{table}[h]
    \caption{Occlusion and Profiler Statistics - 417th frame}
    \centering
    \begin{tabular}{|c|c|c||c|c|c|}
    \hline
         \textbf{SO} & \textbf{SH} & \textbf{DS [KB]} &\textbf{CPU [ms]} &\textbf{GPU [ms]} &\textbf{FPS} \\ \hline\hline
         - & - & - & 22.25 & 11.68 & 44 \\ \hline
         5 & 0.25 & 63.8 & 10.93 & 6.96 & 91\\
         1 & 0.25 & 416.7 & 14.84 & 11.57 & 67\\
         1 & 0.10 & 367.9 & 13.51 & 9.65 & 74\\
         \hline
         \multicolumn{6}{c}{SO = Smallest Occluder, SH = Smallest Hole, DS = Data Size}
    \end{tabular}
    \label{tab:profiler table frame}
\end{table}

\subsubsection{Test 2}

The second approach I took was to choose a location on the Camera's path, where a script  marks the frame number, which I then analysed in the profiler. This way, I compared different stats of the frame but at a similar place. The number of frames, with the FPS, shows how, when optimising, more frames are rendered in a defined time.

In the table, I have written more statistics; the occlusion parameters and the size of the data taken on the disk; the frame that I analysed; the time that both processing units take to process their commands in the frame; the calculated FPS from the frame time; and from the rendering module the number of vertices rendered in the frame and the number of draw calls.

\begin{table}[h]
    \caption{Occlusion and Profiler Statistics}
    \centering
    \begin{tabular}{|c|c|c||c||c|c|c||c|c|}
    \hline
    \textbf{SO} & \textbf{SH} & \textbf{DS [KB]} &\textbf{Frame} &\textbf{CPU [ms]} &\textbf{GPU [ms]} &\textbf{FPS} &\textbf{VC} &\textbf{DC}\\ \hline\hline
         - & - & - & 818 & 19.25 & 19.12 & 51 & 18.3k & 14\\ \hline
         7 & 0.25 & 29.6 & 826 & 19.58 & 19.26 & 51 & 18.3k & 14\\
         5 & 0.25 & 63.8 & 834 & 19.43 & 19.64 & 50 & 18.3k & 14\\
         3 & 0.25 & 123.4 & 841 & 20.38 & 19.32 & 49 & 18.3k & 14\\
         1 & 0.25 & 416.7 & 835 & 31.53 & 21.62 & 31 & 18.3k & 14\\ \hline
         1 & 0.10 & 367.9 & 841 & 19.41 & 19.15 & 51 & 18.3k & 14\\
         3 & 0.50 & 122.0 & 815 & 19.96 & 19.22 & 50 & 18.3k & 14\\
         5 & 0.50 & 47.6 & 840 & 19.31 & 19.15 & 51 & 18.3k & 14\\
         5 & 1.00 & 34.8 & 835 & 19.30 & 19.13 & 51 & 18.3k & 14\\
         \hline\hline
         \multicolumn{9}{|c|}{SO = Smallest Occluder, SH = Smallest Hole, DS = Data Size}\\
         \multicolumn{9}{|c|}{VC = Vertices Count, DC = Draw Calls}\\
         \hline
    \end{tabular}
    \label{tab:profiler table}
\end{table}

\subsection{Results}
The first test had excellent and acceptable results. But as I wanted more samples some days later, the frame times were significantly lower. This is why the first test has fewer samples than the second. 

The second test was made because the results would be more representative of the optimising technique. But in the end, didn't go as well as I had hoped; the real difference was only in the frame captured. We can see better the difference in the frame times than in the FPS.

\chapter{Conclusion}
Occlusion culling is an effective rendering optimising technique by limiting the number of objects that needs to render, optimising a lot of resources that can be used more efficiently by the GPU. The only down side it needs to have baked data to calculate the culling, taking up space on the disk. By wisely choosing its parameters, we can achieve great results. And not always smaller numbers have better results, as changes frequently the GameObjects that need to be rendered.

On the other hand, my demos weren't excellent choices for analysing this optimising technique. Occlusion culling is more effective in high-density scenes with many GameObjects. Large scenes take too long for the data to bake, as experienced in the first demo. In the second demo, the culling was more effective, but the scene was too small, so the frame times were already low, even if not optimised.

\section{Next steps}
For better and more meaningful results, the next scene I will create would be a high-density futuristic city with a large number of building on several floors. My approach will be different, instead of sampling only one frame, I will sample a range of frames where the Camera will travel a defined path, then write down the highest and lowest frame times, more representative results.


\begin{thebibliography}{1}

\bibitem{ebook} Unity Technologies \emph{Optimise your game performance for consoles and PC ebook, 2020 LTS Edition}, 2021

\bibitem{manual} Unity Technologies \emph{Unity Documentation, }\url{https://docs.unity3d.com/Manual/index.html}

\end{thebibliography}

\end{document}

